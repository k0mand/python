print("Zavdanya 1_1")
print("*" * 20)
import random

r = []
for i in range(4):
    k = random.randint(-100, 100)
    r.append(k)
print(r)
s = 0
for i in range(4):
    s = s + r[i]
print("Summa=", s)
print("Zavdanya 1_2")
print("*" * 20)
spys = []
for i in range(21):
    k = random.randint(-100, 100)
    spys.append(k)
print(spys)
print("Zavdanya 1_3")
print("*" * 20)
cities = ['Bud', 'Rome', 'Ista', 'Sydney', 'Kiev', 'Hong']
print(max(cities))
cities.insert(5, 'and')
print(cities)

print("Zavdanya 2")
print("*" * 20)
from random import random

N = 20
a = [0] * N
for i in range(N):
    a[i] = int(random() * 15)
    print(a[i], end=' ')
print()
for i in range(N):
    f = True
    for j in range(N):
        if a[i] == a[j] and i != j:
            f = False
            break
    if f == True:
        print(a[i], end=' ')
print()
print("Zavdanya 3")
print("*" * 20)

import copy


def print_matrix(A):
    for strA in A:
        print(strA)


def minor(A, i, j):
    M = copy.deepcopy(A)
    del M[i]
    for i in range(len(A[0]) - 1):
        del M[i][j]
    return M


def det(A):
    m = len(A)
    n = len(A[0])
    if m != n:
        return None
    if n == 1:
        return A[0][0]
    signum = 1
    determinant = 0
    # разложение по первой строке
    for j in range(n):
        determinant += A[0][j] * signum * det(minor(A, 0, j))
        signum *= -1
    return determinant


A = [[2, 1, 0],
     [1, 5, 0],
     [0, 3, 4]]

print_matrix(A)
print("\n")
print(det(A))