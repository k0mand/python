import datetime


class Student(object):

    def __init__(self, id, lastname, firstname, middlename, dob, address, phone, faculty, course, group):
        """Constructor"""
        self.id = id
        self.lastname = lastname
        self.firstname = firstname
        self.middlename = middlename
        self.dob = datetime.datetime(dob[2], dob[1], dob[0])
        self.address = address
        self.phone = phone
        self.faculty = faculty
        self.course = course
        self.group = group

    def getId(self):
        return self.id

    def setId(self, id):
        self.id = id

    def getLastName(self):
        return self.lastname

    def setLastName(self, lastname):
        self.lastname = lastname

    def getFirstName(self):
        return self.firstname

    def setFirstName(self, firstname):
        self.firstname = firstname

    def getMiddleName(self):
        return self.middlename

    def setMiddleName(self, middlename):
        self.middlename = middlename

    def getDateOfBirthday(self):
        return self.dob

    def setDateOfBirthday(self, dob):
        self.dob = datetime.datetime(dob[2], dob[1], dob[0])

    def getAddress(self):
        return self.address

    def setAddress(self, address):
        self.address = address

    def getPhone(self):
        return self.phone

    def setPhone(self, phone):
        self.phone = phone

    def getFaculty(self):
        return self.faculty

    def setFaculty(self, faculty):
        self.faculty = faculty

    def getCourse(self):
        return self.course

    def setCourse(self, course):
        self.course = course

    def getGroup(self):
        return self.group

    def setGroup(self, group):
        self.group = group


def main():
    array = \
        [
            Student(1, 'Кошик', 'Роман', 'Олександрович', [5, 3, 1999], 'Чорновола 36', '380989965541', 'ФМІ-2', 4,
                    'КТ-41'),
            Student(2, 'Кошик', 'Іван', 'Олександрович', [5, 3, 1999], 'Чорновола 36', '380989965541', 'ФМІ-1', 4,
                    'КТ-41'),
        ]
    cmd = input(
        "Введіть номер завдання: \n"
        "1. Список студентів заданого факультету \n"
        "2. Списки студентів для кожного факультету та курсу \n"
        "3. Список студентів, які народились після заданого року \n"
        "4. Список навчальної групи \n"
    )
    while cmd != "exit":
        if cmd == "1":
            tmp = input("Введіть абревіатуру факультету: ")
            for val in array:
                if tmp == val.getFaculty():
                    print(val.getLastName() + " " + val.getFirstName() + " " + val.getMiddleName())
            print("\b")
        elif cmd == "2":
            array.sort(key=Student.getFaculty, reverse=True)
            for val in array:
                print(val.getLastName() + " " + val.getFirstName() + " " + val.getMiddleName())
            print("\b")
        elif cmd == "3":
            for val in array:
                print('1')
        else:
            print("Введено не вірне значення!")
        cmd = input(
            "Введіть номер завдання: \n"
            "1. Список студентів заданого факультету \n"
            "2. Списки студентів для кожного факультету та курсу \n"
            "3. Список студентів, які народились після заданого року \n"
            "4. Список навчальної групи \n"
        )
    return


main()
