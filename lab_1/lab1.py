print("zavdanya 1")
m=45
f=m*3.98
d=m*39.37
ya=m*1.09
result1= "{0} m ={1:.2f}".format(m,f)
result2= "{0} m ={1:.2f}".format(m,d)
result3= "{0} m ={1:.2f}".format(m,ya)
print(result1)
print(result2)
print(result3)

print("_"*20)
print("zavdanya 2")
dni= 10
hour=24*dni
minut= 60*hour
second=60*minut
print(dni, "//", hour, "//", minut, "//",second )
print("_"*20)
print("zavdanya 3")
C = 45
Far= 32+9/5*C
Kel=C+273.15
print("{0} C ={1:^15.2f}".format(C,Far), "Far")
print("{0} C ={1:^15.2f}".format(C,Kel), "Kel")

print("_"*20)
print("zavdanya 4")


N = 6259
suma = 0
while N>0:
    last = N%10
    suma += last
    N//=10
    a=N /1000
    b=N/ 100
    c=N/10%10
    d=N%10

print("{0} {1:+d} {2:+d} {3:+d}" .format(6,2,5,9),"=" ,suma)

print("_"*20)
print("zavdanya 5")
import math
pi=math.pi
sin=math.sin
cos=math.cos
arccos=math.acos
x1=39.907500
y1=116.3972300
x2=50.4546600
y2=30.523800
x1= x1*pi/180
x2= x2*pi/180
y1= y1*pi/180
y2= y2*pi/180
V=6371.032*arccos(sin(x1)*sin(x2)+cos(x1)*cos(x2)*cos(y1-y2))
print ("{0:>10.3f}". format(V))