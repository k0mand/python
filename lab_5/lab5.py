print("Ноль в качестве знака операции"
      "\nзавершит работу программы")
s = input("Знак (+,-,*,/, %, ^,//): ")

if s in ('+', '-', '*', '/', '%', '^', '//'):
    x = float(input("x="))
    y = float(input("y="))
if s == '+':
    print("%.2f" % (x + y))
elif s == '-':
    print("%.2f" % (x - y))
elif s == '*':
    print("%.2f" % (x * y))
elif s == '^':
    print("%.2f" % (x ** y))
elif s == '/':
    if y != 0:
        print("%.2f" % (x / y))
elif s == '//':
    if y != 0:
        print("%.2f" % (x // y))
elif s == '%':
    print((x % y))
else:
    print("Деление на ноль!")
    print("Неверный знак операции!")

print("Zavd_3")
print("*" * 20)
n = int(input())

b = ''

while n > 0:
    b = str(n % 2) + b
    n = n // 2

print(b)
print("Zavd_4")
print("*" * 20)

s = 'AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC'
print(s)
word_count_dictionary = {}

for c in s:
    if c not in word_count_dictionary:
        word_count_dictionary[c] = 1
    else:
        word_count_dictionary[c] += 1

print(word_count_dictionary)