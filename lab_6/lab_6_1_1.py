class Shop(object):

    def __init__(self, shop_name, store_type, number_of_units=0):
        """Constructor"""
        self.shop_name = shop_name
        self.store_type = store_type
        self.number_of_units = number_of_units

    def describe_shop(self):
        """
        Describe shop
        """
        print(self.shop_name)
        print(self.store_type)

    def open_shop(self):
        """
        Open shop
        """
        print("Shop is open!")

    def set_number_of_units(self, number_of_units):
        """
        Set number of units
        """
        self.number_of_units = number_of_units

    def increment_number_of_units(self, increment):
        """
        Increment number of units
        """
        self.number_of_units = self.number_of_units + increment


class Discount(Shop):

    def __init__(self, shop_name, store_type, discount_products='first, second, third'):
        """Constructor"""
        super().__init__(shop_name, store_type)
        self.discount_products = discount_products

    def get_discounts_products(self):
        """Get discounts products"""
        return self.discount_products


def main():
    cmd = input("Виберіть завдання (a, b, c, d, e) \n")
    while cmd != "exit":
        if cmd == "a":
            """
            A
            """
            shop = Shop("rokos", "game")
            shop.describe_shop()
            shop.open_shop()
        elif cmd == "b":
            """
            B
            """
            first_shop = Shop("rokos_first", "first_type")
            first_shop.describe_shop()
            second_shop = Shop("rokos_second", "second_type")
            second_shop.describe_shop()
            third_shop = Shop("rokos_third", "third_type")
            third_shop.describe_shop()
        elif cmd == "c":
            """
            C
            """
            store = Shop("rokos", "type", 5)
            print(store.number_of_units)
            store.number_of_units = 7
            print(store.number_of_units)
        elif cmd == "d":
            """
            D
            """
            store = Shop("rokos", "type", 5)
            print(store.number_of_units)
            store.set_number_of_units(10)
            print(store.number_of_units)
            store.increment_number_of_units(5)
            print(store.number_of_units)
        elif cmd == "e":
            """
            E
            """
            store_discount = Discount("shop", "type")
            print(store_discount.get_discounts_products())
        else:
            print("Введено не вірне значення!")
        cmd = input("Виберіть завдання (a, b, c, d, e) \n")
    return


main()
