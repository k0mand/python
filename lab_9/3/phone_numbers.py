def main():
    phone_numbers = {
        'Katlien': '+380986562512',
        'Karen': '+380986532517',
        'Konta': '+380986562514',
        'Kanada': '+380986562510',
        'Jein': '+380986162513',
        'Jiny': '+380936562315',
        'Tad': '+380986562510',
        'Rob': '+3809865678514',
        'Matt': '+380789762514'
    }
    with open('phone_numbers.txt', 'w') as f:
        for name in phone_numbers:
            if name[0] == 'K' or name[0] == 'J' or name[0] == 'k' or name[0] == 'j':
                f.write('Name: ' + name + ' ' + 'Phone: ' + phone_numbers[name] + '\n')


main()
