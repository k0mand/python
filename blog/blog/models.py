from django.db import models
from django.utils import timezone

class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class CommentPost(models.Model):
    post = models.ForeignKey(Post, on_delete = models.CASCADE, verbose_name='Статья', blank = True, null = True,related_name='comments_articles' )
    user = models.ForeignKey('auth.User', on_delete = models.CASCADE, verbose_name='Автор комментария', blank = True, null = True )
    created = models.DateTimeField(auto_now=True)
    text = models.TextField(verbose_name='Текст комментария')
    moder = models.BooleanField(verbose_name='Видимость статьи', default=False)

    class Meta:
        verbose_name_plural = 'Коментарі'
        verbose_name = 'Коментар'
        ordering = ['created']